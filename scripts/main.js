"use strict";

$('.owl-carousel').owlCarousel({
  loop: false,
  margin: 5,
  center: true,
  nav: true,
  responsive: {
      0:{
        items:1.2
      },
      1024: {
        items: 2,
        center: false,
        nav: true,
      }
  }
})